# Elder Scrolls Legends Database (ESLDB) - Prototype  #

Made: Summer 2016

An unfinished web app built with .NET Core 1.0 for the Elder Scrolls Legends Game.

Tech stack:
.NET Core 1.0
ASP MVC 6
Entity Framework 7
SQL Server 2016
Bootstrap v3 with jQuery and a bit of Angular.js

## Some example images of ESLDB ##

<img src="https://gitlab.com/wilcoforr/ElderScrollsLegendsDatabase/raw/master/home-screen.png" width="400" height="300" alt="Home Screen" title="Home Screen" />

<img src="https://gitlab.com/wilcoforr/ElderScrollsLegendsDatabase/raw/master/card-list.png" width="400" height="300" alt="Cards as a list of details" title="Cards as a list of details" />

<img src="https://gitlab.com/wilcoforr/ElderScrollsLegendsDatabase/raw/master/cards-as-images.png" width="400" height="300" alt="Cards listed as images" title="Cards listed as images" />

<img src="https://gitlab.com/wilcoforr/ElderScrollsLegendsDatabase/raw/master/card-detail.png" width="400" height="300" alt="Card detail view" title="Card detail view" />


## Database Diagram ##

<img src="https://gitlab.com/wilcoforr/ElderScrollsLegendsDatabase/raw/master/database_diagram.png" width=400 height=300 alt="Database Diagram" title="Database Diagram" />


