﻿//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Drawing;
//using System.Drawing.Imaging;

//namespace ImageThumbnailAutomaker
//{
//    class Program
//    {

//        static void Main(string[] args)
//        {
//            Console.WriteLine("ImageThumbnailAutomaker");
//            Console.WriteLine("Create thumbnails quickly within a directory");
//            Console.WriteLine("version 1.0");
//            Console.WriteLine();

//            Console.WriteLine("Do you wish to create thumbnails of all the images in this directory?" + Environment.NewLine + Environment.CurrentDirectory);
//            Console.WriteLine(Environment.NewLine + "Enter 'y' to generate thumbnails for all .jpg and .png files in this directory");

//            string userInput = Console.ReadLine().ToLower();

//            if (userInput != "y")
//            {
//                Console.WriteLine("Cancelled. Press enter to exit");
//                Console.ReadLine();
//                return;
//            }


//            int THUMBNAIL_WIDTH = 100;
//            int THUMBNAIL_HEIGHT = 100;

//            //either take in the width and height as a command line argument
//            //or ask the user what they want for width and height of thumbnail
//            //perhaps this could be made better if like the dimensions of the image were
//            //continually sized down by like a factor of 2 until both dimensions where below 100px or so
//            if (args.Length == 2)
//            {
//                THUMBNAIL_WIDTH = Convert.ToInt32(args[0]);
//                THUMBNAIL_HEIGHT = Convert.ToInt32(args[1]);
//            }
//            else
//            {
//                try
//                {
//                    Console.WriteLine("Enter the desired WIDTH of the thumbnail: ");
//                    THUMBNAIL_WIDTH = Convert.ToInt32(Console.ReadLine());
//                    Console.WriteLine("Enter the desired HEIGHT of the thumbnail: ");
//                    THUMBNAIL_HEIGHT = Convert.ToInt32(Console.ReadLine());
//                }
//                catch (Exception ex)
//                {
//                    Console.WriteLine(ex.ToString());

//                    Console.ReadLine();
//                    Environment.Exit(1);
//                }
//            }

//            Console.WriteLine("Creating thumbnails....");

//            //List of Filenames that are images - files ending with .jpg, .png, .bmp, or .gif
//            var imageFileNames = new List<string>();

//            //get all file names in the .exe's current directory
//            string[] fileNamesInExeDirectory = Directory.GetFiles(Environment.CurrentDirectory);

//            var validImageFileExtensions = new List<string>();
//            validImageFileExtensions.Add(".png");
//            validImageFileExtensions.Add(".jpg");
//            validImageFileExtensions.Add(".jpeg");
//            validImageFileExtensions.Add(".gif");
//            validImageFileExtensions.Add(".bmp");

//            foreach (var fileName in fileNamesInExeDirectory)
//            {
//                string fileExtension = Path.GetExtension(fileName).ToLower();

//                //only add filenames that have the extension of png, jpg, jpeg, bmp, or gif
//                if (validImageFileExtensions.Contains(fileExtension))
//                {
//                    //avoid duplication of thumbnails
//                    if (!fileName.Contains("_thumb"))
//                    {
//                        //Console.WriteLine("Image added to create a thumbnail of: \t" + fileName);
//                        imageFileNames.Add(fileName);
//                    }
//                }
//            }

//            if (imageFileNames.Count > 0)
//            {
//                Size thumbnailSize = new Size(THUMBNAIL_WIDTH, THUMBNAIL_HEIGHT);

//                foreach (var fileName in imageFileNames)
//                {

//                    using (Image image = Image.FromFile(fileName))
//                    using (var thumbnail = new Bitmap(image, thumbnailSize))
//                    {
//                        string imageExt = Path.GetExtension(fileName).ToLower();

//                        string thumbnailImageFileName = Path.GetFileNameWithoutExtension(fileName) + "_thumb" + imageExt;

//                        switch (imageExt)
//                        {
//                            case ".jpg":
//                            case ".jpeg":
//                                thumbnail.Save(thumbnailImageFileName, ImageFormat.Jpeg);
//                                break;
//                            case ".bmp":
//                                thumbnail.Save(thumbnailImageFileName, ImageFormat.Bmp);
//                                break;
//                            case ".gif":
//                                thumbnail.Save(thumbnailImageFileName, ImageFormat.Gif);
//                                break;
//                            //save as png or new thumb as png by default
//                            case ".png":
//                            default:
//                                thumbnail.Save(thumbnailImageFileName, ImageFormat.Png);
//                                break;
//                        }
//                    }
//                    Console.WriteLine("Created a thumbnail for:\t" + fileName);
//                }

//                Console.WriteLine(Environment.NewLine + "Created all thumbnails!" + Environment.NewLine);

//            }
//            else
//            {
//                Console.WriteLine("No .jpg or .png images found to create a thumbnail of the image.");
//            }

//            Console.WriteLine();
//            Console.WriteLine("Press enter to exit.");

//            Console.ReadLine();

//        }
//    }
//}