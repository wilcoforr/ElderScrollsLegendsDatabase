﻿using System;
using System.Collections.Generic;
using ElderScrollsLegendsDB.Models;

namespace ElderScrollsLegendsDB.Data
{
    /// <summary>
    /// Repository Idea - not really implemented all the way however
    /// Used by EslDbRepository
    /// </summary>
    public interface IElderScrollsLegendsRepository
    {
        List<Card> GetAllCards();

        Card GetCardByID(int cardID);

        void AddCard(Card card);
        void DeleteCard(int cardID);
        void UpdateCard(Card card);
    }
}
