﻿using System;
using System.Collections.Generic;
using ElderScrollsLegendsDB.Models;
using System.ComponentModel.DataAnnotations;

namespace ElderScrollsLegendsDB.ViewModels
{
    /// <summary>
    /// A DeckViewModel represents a View friendly strongly typed model for the View to use
    /// </summary>
    public class DeckViewModel
    {
        public int DeckID { get; set; }

        public string DeckName { get; set; }
        public string DeckDescription { get; set; }

        public int UserID { get; set; }
        public virtual User User { get; set; }

        [DataType(DataType.Date)]
        public DateTime? DateCreated { get; set; }

        [DataType(DataType.Date)]
        public DateTime? DateLastModified { get; set; }



        //CardIDs are used to initially get the Card object
        //they are empty lists but are added to in the controllers by accessing the db
        //perhaps improve.
        public List<int> CardIDs { get; set; }
        public List<Card> Cards { get; set; }


        public DeckViewModel()
        {
            CardIDs = new List<int>();
            Cards = new List<Card>();
        }

        /// <summary>
        /// Construct a DeckViewModel based on a Deck
        /// DeckViewModel is used in view of course
        /// </summary>
        /// <param name="deck"></param>
        public DeckViewModel(Deck deck)
        {
            DeckID = deck.DeckID;
            DeckName = deck.DeckName;
            DeckDescription = deck.DeckDescription;
            DateCreated = deck.DateCreated;
            DateLastModified = deck.DateLastModified;

            User = deck.User;

            CardIDs = new List<int>();
            Cards = new List<Card>();
        }

    }
}
