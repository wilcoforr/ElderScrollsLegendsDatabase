﻿using System.Collections.Generic;
using ElderScrollsLegendsDB.Models;
using System.ComponentModel.DataAnnotations;

namespace ElderScrollsLegendsDB.ViewModels
{
    /// <summary>
    /// Mainly used for the creation and editing of cards so far
    /// </summary>
    public class CardViewModel
    {
        //used for updating cards
        [Display(Name = "Card ID")]
        public int CardID { get; set; }

        [Display(Name = "Card Name")]
        public string CardName { get; set; }

        [Display(Name = "Card Description")]
        public string CardDescription { get; set; }

        [Display(Name = "Mana Cost")]
        public int ManaCost { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }

        public int AttributeID { get; set; }
        [Display(Name = "Attribute")]
        public List<Attribute> AttributeTypes { get; set; }

        public int CardTypeID { get; set; }
        [Display(Name = "Card Type")]
        public List<CardType> CardTypes { get; set; }

        public int RarityID { get; set; }
        [Display(Name = "Rarity")]
        public List<Rarity> RarityTypes { get; set; }


        public string ArtFullResFileName { get; set; }
        public string ArtThumbnailFileName { get; set; }


        public CardViewModel()
        {
            AttributeTypes = new List<Attribute>();
            CardTypes = new List<CardType>();
            RarityTypes = new List<Rarity>();
        }

        /// <summary>
        /// Create a CardViewModel object based on a card
        /// Mainly for use with Edit/Creation of cards
        /// </summary>
        /// <param name="card">Card model</param>
        public CardViewModel(Card card)
        {
            AttributeTypes = new List<Attribute>();
            CardTypes = new List<CardType>();
            RarityTypes = new List<Rarity>();

            //CardID = card.CardID;
            //ArtFullResFileName = card.ArtFullResFileName;
            //ArtThumbnailFileName = card.ArtThumbnailFileName;

            CardName = card.CardName;
            CardDescription = card.CardDescription;
            ManaCost = card.ManaCost;
            Attack = card.Attack;
            Defense = card.Defense;

            AttributeID = card.AttributeID;
            CardTypeID = card.CardTypeID;
            RarityID = card.RarityID;
        }

    }
}
