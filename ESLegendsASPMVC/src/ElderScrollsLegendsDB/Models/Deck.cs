﻿
using System;

namespace ElderScrollsLegendsDB.Models
{
    public class Deck
    {
        public int DeckID { get; set; }
        public string DeckName { get; set; }
        public string DeckDescription { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateLastModified { get; set; }

        public int UserID { get; set; }
        public virtual User User { get; set; }
    }
}
