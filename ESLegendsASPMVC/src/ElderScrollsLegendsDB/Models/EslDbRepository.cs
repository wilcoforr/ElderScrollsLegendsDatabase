﻿
using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ElderScrollsLegendsDB.Models
{
    /// <summary>
    /// a repo that has the .GetCards() and related methods - instead of them being inside the controller.
    /// this way all controllers can use this code instead of just copying-pasting similar code.
    /// </summary>
    public class EslDbRepository : Data.IElderScrollsLegendsRepository
    {
        private ApplicationDbContext _context;

        // from controller - dunno if this injection? works?
        public EslDbRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Add a new Card to the database
        /// </summary>
        /// <param name="card"></param>
        public void AddCard(Card card)
        {
            _context.Card.Add(card);
            _context.SaveChanges();
        }

        /// <summary>
        /// Delete a card by CardID from the database
        /// </summary>
        /// <param name="cardID"></param>
        public void DeleteCard(int cardID)
        {
            Card cardToDelete = _context.Card.Where(c => c.CardID == cardID).FirstOrDefault();

            if(cardToDelete != null)
            {
                _context.Card.Remove(cardToDelete);
                _context.SaveChanges();
            }
            else
            {
                throw new Exception("Card not found. CardID: " + cardID);
            }
        }

        /// <summary>
        /// Get all cards from the database
        /// </summary>
        /// <returns>All cards as a List</returns>
        public List<Card> GetAllCards()
        {
            return _context.Card.Include(c => c.Attribute)
                                .Include(c => c.CardType)
                                .Include(c => c.Rarity)
                                .ToList();
        }

        /// <summary>
        /// Get a card by CardID from the database
        /// </summary>
        /// <param name="cardID"></param>
        /// <returns></returns>
        public Card GetCardByID(int cardID)
        {
            return _context.Card.Include(c => c.Attribute)
                                .Include(c => c.CardType)
                                .Include(c => c.Rarity)
                                .Where(c => c.CardID == cardID)
                                .FirstOrDefault();
        }

        /// <summary>
        /// Update a Card in the database
        /// </summary>
        /// <param name="card"></param>
        public void UpdateCard(Card card)
        {
            _context.Update(card);
            _context.SaveChanges();
        }

    }
}
