﻿namespace ElderScrollsLegendsDB.Models
{
    public class DeckToCardsMap
    {
        [System.ComponentModel.DataAnnotations.Key]
        public int DeckToCardsMapID { get; set; }
        public int DeckID { get; set; }
        public int CardID { get; set; }
    }
}