﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;

namespace ElderScrollsLegendsDB.Models
{
    public class ApplicationDbContext : DbContext
    {

        public virtual DbSet<Card> Card { get; set; }
        public virtual DbSet<Rarity> Rarity { get; set; }
        public virtual DbSet<Attribute> Attribute { get; set; }
        public virtual DbSet<CardType> CardType { get; set; }

        public virtual DbSet<Deck> Deck { get; set; }
        public virtual DbSet<DeckToCardsMap> DeckToCardsMap { get; set;}

        //User still left mostly unimplemented
        //ASP.Net Identity would be used instead of this (my "roll your own" registration/auth)
        public virtual DbSet<User> User { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Card>(entity =>
            {
                entity.Property(e => e.CardName).IsRequired();

                //entity.HasOne(c => c.Rarity).WithMany(r => r.CardID);
            });
        }



    }
}
