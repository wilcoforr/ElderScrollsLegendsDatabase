﻿namespace ElderScrollsLegendsDB.Models
{
    public class CardType
    {
        public int CardTypeID { get; set; }
        public string CardTypeValue { get; set; }
    }
}
