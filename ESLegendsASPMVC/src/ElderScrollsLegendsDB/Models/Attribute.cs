﻿namespace ElderScrollsLegendsDB.Models
{
    public class Attribute
    {
        public int AttributeID { get; set; }
        public string AttributeType { get; set; }
    }
}
