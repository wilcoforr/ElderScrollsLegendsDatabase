﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ElderScrollsLegendsDB.Models
{
    public class Rarity
    {
        public int RarityID { get; set; }
        public string RarityType { get; set; }
    }
}
