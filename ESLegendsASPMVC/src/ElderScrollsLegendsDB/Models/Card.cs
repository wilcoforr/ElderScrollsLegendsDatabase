﻿using System.ComponentModel.DataAnnotations.Schema;

namespace ElderScrollsLegendsDB.Models
{
    public class Card
    {
        public int CardID { get; set; }
        public string CardName { get; set; }

        //action, nord, dwemer, dragon, imperial, etc
        public int CardTypeID { get; set; }
        public virtual CardType CardType { get; set; }

        //agility = green, etc
        public int AttributeID { get; set; }
        public virtual Attribute Attribute { get; set; }

        //combat attributes
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int ManaCost { get; set; }

        public int RarityID { get; set; }

        //[ForeignKey("RarityID")]
        public virtual Rarity Rarity { get; set; }

        //lore
        public string FlavorLoreText { get; set; }

        //card effect
        //  "Deal 1 damage to all creatures in this lane when summoned."
        public string CardDescription { get; set; }

        //file names for the images
        //  nordBerserker.jpg
        //  nordBerserker_thumb.jpg
        public string ArtFullResFileName { get; set; }
        public string ArtThumbnailFileName { get; set; }

    }
}
