﻿using Microsoft.AspNet.Mvc;

namespace ElderScrollsLegendsDB.Controllers
{
    public class ClassesController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Archer()
        {
            return View();
        }

        public IActionResult Assassin()
        {
            return View();
        }

        public IActionResult Battlemage()
        {
            return View();
        }

        public IActionResult Crusader()
        {
            return View();
        }

        public IActionResult Mage()
        {
            return View();
        }

        public IActionResult Monk()
        {
            return View();
        }

        public IActionResult Scout()
        {
            return View();
        }

        public IActionResult Sorceror()
        {
            return View();
        }

        public IActionResult Spellsword()
        {
            return View();
        }

        public IActionResult Warrior()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
