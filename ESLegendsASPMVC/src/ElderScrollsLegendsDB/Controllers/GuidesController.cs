﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace ElderScrollsLegendsDB.Controllers
{
    public class GuidesController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            return View();
        }

        [ActionName("Understanding-Lanes")]
        public IActionResult UnderstandingLanes()
        {
            return View();
        }

        [ActionName("Arena-Tickets")]
        public IActionResult ArenaTickets()
        {
            return View();
        }

        [ActionName("Card-Packs")]
        public IActionResult CardPacks()
        {
            return View();
        }


    }
}
