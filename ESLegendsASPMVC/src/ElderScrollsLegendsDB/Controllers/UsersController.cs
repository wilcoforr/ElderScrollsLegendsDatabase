﻿using System;
using Microsoft.AspNet.Mvc;
using ElderScrollsLegendsDB.Models;
using System.Linq;
using System.Collections.Generic;


namespace ElderScrollsLegendsDB.Controllers
{
    public class UsersController : Controller
    {
        private ApplicationDbContext _context;

        public UsersController(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Profile/Home page for user
        /// Or display a registration page if the user doesnt exist
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            //var model = _context.User.Where(u => u.UserID == 1).FirstOrDefault();

            return View(null);
        }
    }
}
