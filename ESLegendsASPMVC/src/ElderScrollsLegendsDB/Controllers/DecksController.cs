﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Mvc;
using ElderScrollsLegendsDB.Models;
using ElderScrollsLegendsDB.ViewModels;

using Microsoft.Data.Entity;

namespace ElderScrollsLegendsDB.Controllers
{
    public class DecksController : Controller
    {

        private ApplicationDbContext _context;

        public DecksController(ApplicationDbContext context)
        {
            _context = context;
        }


        /// <summary>
        /// Build a basic DeckViewModel based on a deckID
        /// Does NOT add card objects.
        /// </summary>
        /// <param name="deckID"></param>
        /// <returns></returns>
        public DeckViewModel DeckBuilder(Deck deck)
        {

            var deckViewModel = new DeckViewModel(deck);

            //get all cardIDs that belong to this deckID
            var deckToCardsMap = _context.DeckToCardsMap.Where(d => d.DeckID == deckViewModel.DeckID);

            foreach (var deckToCard in deckToCardsMap)
            {

                deckViewModel.CardIDs.Add(deckToCard.CardID);
            }

            return deckViewModel;
        }



        // GET: /<controller>/
        /// <summary>
        /// Get all DeckIDs then build a DeckViewModel then add it to a list for view to display each decks
        /// a deck's cards are not generated here. Just the IDs are added
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            var allDeckIDs = _context.Deck.ToList().Select(d => d.DeckID);

            var model = new List<DeckViewModel>();
            foreach (var deckID in allDeckIDs)
            {
                var deck = _context.Deck
                                    .Include(d => d.User)
                                    .Where(d => d.DeckID == deckID).FirstOrDefault();

                if(deck == null)
                {
                    return HttpNotFound();
                }

                var deckVM = DeckBuilder(deck);

                model.Add(deckVM);
            }

            return View(model);
        }



        /// <summary>
        /// Details of a Deck
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            //cards added here not in deck builder for slight optimization
            if (id > 0 && ModelState.IsValid)
            {
                //int? to int::deckID
                int deckID = id ?? 1;

                var deck = _context.Deck.Where(d => d.DeckID == deckID).FirstOrDefault();

                //if no deck was found ie like a deckID is not in the database
                if (deck == null)
                {
                    return HttpNotFound();
                }

                DeckViewModel deckVM = DeckBuilder(deck);

                //build each card to add to the deck.Cards Card collection
                foreach (var cardID in deckVM.CardIDs)
                {
                    var card = _context.Card.Where(c => c.CardID == cardID)
                        .Include(c => c.Attribute)
                        .Include(c => c.CardType)
                        .Include(c => c.Rarity)
                        .FirstOrDefault();

                    deckVM.Cards.Add(card);
                }

                deckVM.Cards = deckVM.Cards.OrderBy(c => c.CardName).ToList();

                return View(deckVM);
            }
            else
            {
                return View("Error");
            }
        }

    }
}



