﻿using System;
using Microsoft.AspNet.Mvc;
using ElderScrollsLegendsDB.Models;
using System.Linq;
using System.Collections.Generic;
using ElderScrollsLegendsDB.ViewModels;

using Microsoft.Data.Entity;

namespace ElderScrollsLegendsDB.Controllers
{
    public class CardsController : Controller
    {

        private ApplicationDbContext _context;

        //private EslDbRepository eslDbRepo;

        public CardsController(ApplicationDbContext context)
        {
            _context = context;
            
            //EslDbRepo way of doing things... 
            //eslDbRepo = new EslDbRepository(context);
        }


        /// <summary>
        /// DEV ONLY - automation of creating a card's file name
        /// Example:
        /// card name: Dark Elf Wizard
        /// card image name generated: Dark-Elf-Wizard.jpg
        /// </summary>
        public void GenerateCardArtFileNameToDatabase()
        {
            var model = GetAllCards();

            //get all image file names from wwwroot\images
            var imageNames = System.IO.Directory.GetFiles("wwwroot\\images\\cards");

            foreach (var card in model)
            {
                //if (String.IsNullOrEmpty(card.ArtFullResFileName))
                //{
                if (imageNames.Contains("wwwroot\\images\\cards\\" + CreateArtFullResFileName(card.CardName)))
                {
                    card.ArtFullResFileName = CreateArtFullResFileName(card.CardName);
                    card.ArtThumbnailFileName = CreateArtThumbnailFileName(card.CardName);

                    _context.Update(card);
                    _context.SaveChanges();
                }
                else
                {
                    card.ArtFullResFileName = "aaa_template.jpg";
                    card.ArtThumbnailFileName = "aaa_template_thumb.jpg";

                    _context.Update(card);
                    _context.SaveChanges();
                }
                //}
            }
        }


        /// <summary>
        /// The main index page - displays all cards in the database as a list of details or a list of images 
        /// </summary>
        /// <param name="sortOrder"></param>
        /// <param name="searchQuery"></param>
        /// <returns></returns>
        public IActionResult Index(string sortOrder, string searchQuery)
        {
            //#warning GenerateCardArtFileNameToDatabase wew lad
            GenerateCardArtFileNameToDatabase();

            var model = new List<Card>();

            model = GetAllCards();


            //search
            if (!String.IsNullOrEmpty(searchQuery))
            {
                searchQuery = searchQuery.ToLower();

                model = model.Where(c => c.CardName.ToLower().Contains(searchQuery)).ToList();
            }

            #region Sorting
            //ternary toggles for sorting
            ViewBag.SortByName =            (String.IsNullOrEmpty(sortOrder) || sortOrder == "ByName_asc") ? "ByName_desc" : "ByName_asc";
            ViewBag.SortByAttributeType =   (sortOrder == "ByAttributeType_asc") ? "ByAttributeType_desc" : "ByAttributeType_asc";
            ViewBag.SortByCardType =        (sortOrder == "ByCardType_asc") ? "ByCardType_desc" : "ByCardType_asc";
            ViewBag.SortByManaCost =        (sortOrder == "ByManaCost_asc") ? "ByManaCost_desc" : "ByManaCost_asc";
            ViewBag.SortByAttack =          (sortOrder == "ByAttack_asc") ? "ByAttack_desc" : "ByAttack_asc";
            ViewBag.SortByDefense =         (sortOrder == "ByDefense_asc") ? "ByDefense_desc" : "ByDefense_asc";
            ViewBag.SortByRarityType =      (sortOrder == "ByRarityType_asc") ? "ByRarityType_desc" : "ByRarityType_asc";

            //Sort based on the sort order
            //TODO: make someting get all cards and return the result via JSON - then do the sorting on the
            //clients browser to save lots of CPU time.
            //Sorting here usually takes the sort requested (like attribute) then sorts based on that then by card.CardName
            switch (sortOrder)
            {
                case "ByName_asc":
                    model = model.OrderBy(c => c.CardName).ToList();
                    break;
                case "ByName_desc":
                    model = model.OrderByDescending(c => c.CardName).ToList();
                    break;
                case "ByAttributeType_asc":
                    model = model.OrderBy(c => c.Attribute.AttributeType).ThenBy(c => c.CardName).ToList();
                    break;
                case "ByAttributeType_desc":
                    model = model.OrderByDescending(c => c.Attribute.AttributeType).ThenBy(c => c.CardName).ToList();
                    break;
                case "ByCardType_asc":
                    model = model.OrderBy(c => c.CardType.CardTypeValue).ThenBy(c => c.CardName).ToList();
                    break;
                case "ByCardType_desc":
                    model = model.OrderByDescending(c => c.CardType.CardTypeValue).ThenBy(c => c.CardName).ToList();
                    break;
                case "ByManaCost_asc":
                    model = model.OrderBy(c => c.ManaCost).ThenBy(c => c.CardName).ToList();
                    break;
                case "ByManaCost_desc":
                    model = model.OrderByDescending(c => c.ManaCost).ThenBy(c => c.CardName).ToList();
                    break;
                case "ByAttack_asc":
                    model = model.OrderBy(c => c.Attack).ThenBy(c => c.CardName).ToList();
                    break;
                case "ByAttack_desc":
                    model = model.OrderByDescending(c => c.Attack).ThenBy(c => c.CardName).ToList();
                    break;
                case "ByDefense_asc":
                    model = model.OrderBy(c => c.Defense).ThenBy(c => c.CardName).ToList();
                    break;
                case "ByDefense_desc":
                    model = model.OrderByDescending(c => c.Defense).ThenBy(c => c.CardName).ToList();
                    break;
                //using RarityID for a proper sort
                case "ByRarityType_asc":
                    model = model.OrderBy(c => c.RarityID).ThenBy(c => c.CardName).ToList();
                    break;
                case "ByRarityType_desc":
                    model = model.OrderByDescending(c => c.RarityID).ThenBy(c => c.CardName).ToList();
                    break;
                default:
                    model = model.OrderBy(c => c.CardName).ToList();
                    break;
            }

            #endregion

            return View(model);
        }


        /// <summary>
        /// Get All Cards
        /// </summary>
        /// <returns>List of Cards from the database</returns>
        public List<Card> GetAllCards()
        {
            //using Microsoft.Data.Entity; -- the Include ext method for fk
            var cards = _context.Card.Include(c => c.Attribute)
                                    .Include(c => c.CardType)
                                    .Include(c => c.Rarity)
                                    .ToList();

            return cards;
        }


        /// <summary>
        /// Calls the GetAllCards() method then returns that List of type Card as JSON 
        /// </summary>
        /// <returns></returns>
        public JsonResult GetAllCardsAsJson()
        {
            return Json(GetAllCards());
        }


        /// <summary>
        /// Returns a single Card object by its CardID
        /// </summary>
        /// <param name="cardID"></param>
        /// <returns></returns>
        public Card GetCardByCardID(int cardID)
        {
            //using Microsoft.Data.Entity; -- the Include ext method
            var card = _context.Card.Where(c => c.CardID == cardID)
                                    .Include(c => c.Attribute)
                                    .Include(c => c.CardType)
                                    .Include(c => c.Rarity)
                                    .FirstOrDefault();

            return card;
        }


        /// <summary>
        /// remove space, comma, and quote ' char from name then trim it. then append .jpg
        /// </summary>
        /// <returns>a string to be used as the full res art image filename</returns>
        public string CreateArtFullResFileName(string cardName)
        {
            cardName = cardName.Replace(' ', '-').Replace('\'', '-').Replace(',', '-').Trim();

            cardName += ".jpg";

            return cardName;
        }

        /// <summary>
        /// remove space, comma, and quote ' char from name then trim it. then append _thumb.jpg
        /// </summary>
        /// <returns>a string to be used as the thumbnail art image filename</returns>
        public string CreateArtThumbnailFileName(string cardName)
        {
            cardName = cardName.Replace(' ', '-').Replace('\'', '-').Replace(',', '-').Trim();

            cardName += "_thumb.jpg";

            return cardName;
        }



        /// <summary>
        /// Shows the details of a Card
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            //set CardID to 0 if null
            int cardID = id ?? 0;

            if (id > 0 && ModelState.IsValid)
            {
                //Card card = _context.Card.Single(c => c.CardID == id);

                Card card = GetCardByCardID(cardID);

                if (card == null)
                {
                    return View("NotFound");
                }

                return View(card);
            }
            else
            {
                return View("Error");
            }
        }


        /// <summary>
        /// Create a card to add to database form
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
#warning for local dev, to add cards to the db, comment line below this and uncomment the other parts below and in edit view
            //return RedirectToAction("Index");

            var cardViewModel = new CardViewModel();

            cardViewModel.AttributeTypes.AddRange(_context.Attribute.ToList());
            cardViewModel.CardTypes.AddRange(_context.CardType.ToList());
            cardViewModel.RarityTypes.AddRange(_context.Rarity.ToList());

            return View(cardViewModel);
        }


        /// <summary>
        /// save the new card to the database
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CardViewModel viewModel)
        {
            //add and save card to database
            if (ModelState.IsValid)
            {
                var card = new Card();

                card.CardName = viewModel.CardName;
                card.CardDescription = viewModel.CardDescription;
                card.ManaCost = viewModel.ManaCost;
                card.Attack = viewModel.Attack;
                card.Defense = viewModel.Defense;

                card.AttributeID = viewModel.AttributeID;
                card.CardTypeID = viewModel.CardTypeID;
                card.RarityID = viewModel.RarityID;

                _context.Card.Add(card);
                _context.SaveChanges();

                return RedirectToAction("Index");
            }

            return View(viewModel);
        }




        /// <summary>
        /// Edit the card
        /// Commented out because editing cards is only useful when initially adding cards to the db
        /// </summary>
        /// <param name="id">Card's ID</param>
        /// <returns></returns>
        public IActionResult Edit(int? id)
        {
            if (id == null || id < 1)
            {
                return HttpNotFound();
            }

            Card card = _context.Card.Single(m => m.CardID == id);
            if (card == null)
            {
                return HttpNotFound();
            }

            CardViewModel viewModel = new CardViewModel(card);

            viewModel.AttributeTypes.AddRange(_context.Attribute.ToList());
            viewModel.CardTypes.AddRange(_context.CardType.ToList());
            viewModel.RarityTypes.AddRange(_context.Rarity.ToList());

            viewModel.CardID = card.CardID;

            return View(viewModel);

        }



        /// <summary>
        /// Commented out because editing cards is only useful when initially adding cards to the db
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(CardViewModel viewModel)
        {
            //add and save card to database
            if (ModelState.IsValid)
            {
                var card = new Card();

                card.CardID = viewModel.CardID;
                card.CardName = viewModel.CardName;
                card.CardDescription = viewModel.CardDescription;
                card.ManaCost = viewModel.ManaCost;
                card.Attack = viewModel.Attack;
                card.Defense = viewModel.Defense;

                card.AttributeID = viewModel.AttributeID;
                card.CardTypeID = viewModel.CardTypeID;
                card.RarityID = viewModel.RarityID;

                _context.Update(card);
                _context.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(viewModel);
        }

        public IActionResult Delete(int? id)
        {
            if (id == null || id < 1)
            {
                return HttpNotFound();
            }

            Card card = _context.Card.Single(m => m.CardID == id);
            if (card == null)
            {
                return HttpNotFound();
            }

            try
            {
                _context.Remove(card);
                _context.SaveChanges();
            }
            catch (Exception)
            {
                return View("Error");
                //throw;
            }

            return View(card);

        }



    }
}
