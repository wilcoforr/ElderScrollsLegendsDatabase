﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

namespace ElderScrollsLegendsDB.Controllers
{
    public class AttributesController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Agility()
        {
            return View();
        }

        public IActionResult Endurance()
        {
            return View();
        }

        public IActionResult Intelligence()
        {
            return View();
        }

        public IActionResult Strength()
        {
            return View();
        }

        public IActionResult Willpower()
        {
            return View();
        }

        public IActionResult Neutral()
        {
            return View();
        }

        [ActionName("Dual-Attribute")]
        public IActionResult DualAttribute()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}



