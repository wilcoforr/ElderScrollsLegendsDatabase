﻿using Microsoft.AspNet.Mvc;

namespace ElderScrollsLegendsDB.Controllers
{
    public class RacesController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Argonian()
        {
            return View();
        }

        public IActionResult Breton()
        {
            return View();
        }

        [ActionName("Dark-Elf")]
        public IActionResult DarkElf()
        {
            return View();
        }

        [ActionName("High-Elf")]
        public IActionResult HighElf()
        {
            return View();
        }

        public IActionResult Imperial()
        {
            return View();
        }

        public IActionResult Khajiit()
        {
            return View();
        }

        public IActionResult Nord()
        {
            return View();
        }

        public IActionResult Orc()
        {
            return View();
        }

        public IActionResult Redguard()
        {
            return View();
        }

        [ActionName("Wood-Elf")]
        public IActionResult WoodElf()
        {
            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
