﻿SET IDENTITY_INSERT [dbo].[Card] ON 

GO

--INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (1, N'PLACE_HOLDER', 10, 4, 99, 99, 99, 4, N'', N'Place Holder card for when card doesnt exist or whatever.', N'aaa_template.jpg', N'aaa_template_thumb.jpg')
--GO

INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (1, N'Nord Firebrand', 10, 4, 1, 2, 0, 1, N'', N'Charge', N'Nord-Firebrand.jpg', N'Nord-Firebrand_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (2, N'Covenant Marauder', 11, 4, 2, 1, 1, 2, N'', N'+2/+0 when you have no cards in your hand.', N'Covenant-Marauder.jpg', N'Covenant-Marauder_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (3, N'Fiery Imp', 15, 4, 1, 1, 1, 1, N'', N'When Fiery Imp is summoned, deal 2 damage to your opponent.', N'Fiery-Imp.jpg', N'Fiery-Imp_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (4, N'Intimidate', 1, 4, 0, 0, 1, 1, N'', N'Enemy creatures lose Guard.', N'aaa_template.jpg', N'aaa_template_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (5, N'Morthal Watchman', 10, 4, 2, 1, 1, 1, N'', N'', N'Morthal-Watchman.jpg', N'Morthal-Watchman_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (6, N'Rapid Shot', 1, 4, 0, 0, 1, 1, N'', N'Deal 1 damage to a creature. If it survives, draw a card.', N'Rapid-Shot.jpg', N'Rapid-Shot_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (7, N'Relentless Raider', 10, 4, 2, 1, 1, 3, N'', N'When an enemy rune is destroyed, deal 1 damage to your opponent.', N'Relentless-Raider.jpg', N'Relentless-Raider_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (8, N'Scuttler', 14, 4, 1, 2, 1, 1, N'', N'', N'aaa_template.jpg', N'aaa_template_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (9, N'Sharpshooter Scout', 13, 4, 1, 1, 1, 1, N'', N'Prophecy Summon: Deal 1 damage.', N'Sharpshooter-Scout.jpg', N'Sharpshooter-Scout_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (10, N'Steel Scimitar', 2, 4, 1, 1, 1, 1, N'', N'+2/+2', N'Steel-Scimitar.jpg', N'Steel-Scimitar_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (11, N'Blood Dragon', 16, 4, 5, 7, 5, 4, N'', N'Blood dragon ignores Guards and can attack creatures in any lane.', N'Blood-Dragon.jpg', N'Blood-Dragon_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (12, N'Fortress Watchman', 11, 4, 5, 4, 5, 2, N'', N'Guard.', N'aaa_template.jpg', N'aaa_template_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (13, N'Grahtwood Ambusher', 13, 4, 4, 2, 5, 3, N'', N'Prophecy Summon: Deal 1 damage to all enemy creatures in this lane.', N'aaa_template.jpg', N'aaa_template_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (14, N'Markarth Bannerman', 10, 4, 4, 4, 5, 3, N'', N'When Markarth Bannerman attacks, draw two  1/1 Nord Firebrands with Charge.', N'aaa_template.jpg', N'aaa_template_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (15, N'Reive, Blademaster', 15, 4, 4, 4, 5, 4, N'', N'Summon: The next item you equip this turn is free. Reive has a +1/+1 for each item he has equipped.', N'aaa_template.jpg', N'aaa_template_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (16, N'Triumphant Jarl', 10, 4, 4, 4, 5, 3, N'', N'Summon: If you have more health than your opponent, draw two cards.', N'aaa_template.jpg', N'aaa_template_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (17, N'Wood Orc Headhunter', 11, 4, 5, 4, 5, 3, N'', N'Breakthrough Summon: Give Wood Orc Headhunter Charge if you have another friendly orc.', N'aaa_template.jpg', N'aaa_template_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (18, N'Burn and Pillage', 1, 4, 0, 0, 6, 4, N'', N'Deal 1 damage to all enemy creatures for each destroyed enemy rune.', N'aaa_template.jpg', N'aaa_template_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (19, N'Child of Hircine', 19, 4, 6, 7, 6, 3, N'', N'When Child of Hircine attacks and destroys a creature, it may attack again this turn.', N'aaa_template.jpg', N'aaa_template_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (20, N'Nimble Ally', 13, 1, 3, 3, 3, 1, NULL, N'Summon: +1/+1 and Lethal if the top card of your deck is a O.', N'Nimble-Ally.jpg', N'Nimble-Ally_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (21, N'Skooma Racketeer', 9, 1, 2, 2, 3, 2, NULL, N'Summon: Give a creature Lethal.', N'Skooma-Racketeer.jpg', N'Skooma-Racketeer_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (22, N'Swift Strike', 1, 1, 0, 0, 3, 2, NULL, N'Give a creature an extra attack this turn.', N'Swift-Strike.jpg', N'Swift-Strike_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (23, N'Thievery', 1, 1, 0, 0, 3, 1, NULL, N'Deal 3 damage to your opponent and gain 3 health.', N'Thievery.jpg', N'Thievery_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (24, N'Varanis Courier', 6, 1, 1, 3, 3, 1, NULL, N'Guard Last Grasp: Draw a card.', N'Varanis-Courier.jpg', N'Varanis-Courier_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (25, N'Arrow Storm', 1, 5, 0, 0, 3, 2, NULL, N'Destroy all enemy creatures with 2 power or less in one lane.', N'Arrow-Storm.jpg', N'Arrow-Storm_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (26, N'Artaeum Savant', 7, 5, 2, 4, 3, 3, NULL, N'When you play an action, a random friendly creature gets +1/+1.', N'Artaeum-Savant.jpg', N'Artaeum-Savant_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (27, N'Dawnstar Healer', 10, 5, 3, 3, 3, 2, NULL, N'When an enemy rune is destroyed, gain 3 health.', N'Dawnstar-Healer.jpg', N'Dawnstar-Healer_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (28, N'Imperial Legionnaire', 8, 5, 3, 4, 3, 1, NULL, NULL, N'Imperial-Legionnaire.jpg', N'Imperial-Legionnaire_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (29, N'Priest of the Moons', 9, 5, 2, 2, 2, 1, NULL, N'Prophecy Summon: Gain 2 health.', N'Priest-of-the-Moons.jpg', N'Priest-of-the-Moons_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (30, N'Ravenous Hunger', 15, 5, 4, 1, 2, 3, NULL, N'Summon: Give Ravenous Hunger Drain if there is an enemy creature in this lane.', N'Ravenous-Hunger.jpg', N'Ravenous-Hunger_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (31, N'Skingrad Soldier', 8, 5, 2, 3, 2, 1, NULL, N'Guard', N'Skingrad-Soldier.jpg', N'Skingrad-Soldier_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (32, N'Abecean Navigator', 7, 3, 3, 2, 2, 1, NULL, N'Summon: If the top card of your deck is an action, draw it. Otherwise, put it on the bottom.', N'Abecean-Navigator.jpg', N'Abecean-Navigator_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (33, N'Balmora Spymaster', 6, 3, 0, 1, 2, 3, NULL, N'Last Gasp: Summon a random creature with cost less than or equal to your max magicka.', N'Balmora-Spymaster.jpg', N'Balmora-Spymaster_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (34, N'Brilliant Explosion', 1, 3, 0, 0, 2, 3, NULL, N'Draw a copy of a creature.', N'aaa_template.jpg', N'aaa_template_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (35, N'Brutal Ashlander', 6, 3, 1, 1, 1, 1, NULL, N'Last Gasp: Deal 3 Damage to a random Enemy', N'Brutal-Ashlander.jpg', N'Brutal-Ashlander_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (36, N'Crown Quartermaster', 12, 3, 2, 1, 1, 1, NULL, N'Summon: Draw a +1/+0 Steel Dagger.', N'Crown-Quartermaster.jpg', N'Crown-Quartermaster_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (37, N'Cruel Firebloom', 1, 3, 0, 0, 0, 2, NULL, N'Sacrifice a creature to deal 5 damage to a random enemy creature.', N'Cruel-Firebloom.jpg', N'Cruel-Firebloom_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (38, N'Crystal Tower Crafter', 7, 3, 2, 2, 2, 1, NULL, N'When you play an action, Crystal Tower Crafter gains +1/+1/.', N'Crystal-Tower-Crafter.jpg', N'Crystal-Tower-Crafter_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (39, N'Firebolt', 1, 3, 0, 0, 1, 1, NULL, N'Deal 2 damage.', N'aaa_template.jpg', N'aaa_template_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (40, N'Lesser Ward', 1, 3, 0, 0, 0, 1, NULL, N'Give a creature Ward.', N'Lesser-Ward.jpg', N'Lesser-Ward_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (41, N'Niben Bay Cuttroat', 12, 3, 1, 1, 1, 2, NULL, N'+2/+0 when there are no enemy creatures in this lane.', N'aaa_template.jpg', N'aaa_template_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (42, N'Barded Guar', 14, 6, 3, 3, 3, 1, NULL, N'Summon: Give a creature Guard.', N'Barded-Guar.jpg', N'Barded-Guar_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (43, N'Crushing Blow', 1, 6, 0, 0, 3, 1, NULL, N'Deal 3 damage.', N'Crushing-Blow.jpg', N'Crushing-Blow_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (44, N'Dwarven Sphere', 17, 6, 2, 3, 3, 1, NULL, N'Summon: Shackle an enemy creature.', N'Dwarven-Sphere.jpg', N'Dwarven-Sphere_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (45, N'Elixir of Conflict', 3, 6, 0, 0, 3, 2, NULL, N'Uses: 3 Activate: Give a creature +1/+1.', N'Elixir-of-Conflict.jpg', N'Elixir-of-Conflict_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (46, N'Forsword Guide', 18, 6, 1, 1, 3, 2, NULL, N'Summon: Return another friendly creature to your hand to give Forsworn Guide +2/+2.', N'aaa_template.jpg', N'aaa_template_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (47, N'Lucien Lachance', 8, 2, 1, 5, 4, 4, NULL, N'Lethal When a friendly creature attacks and destroys a creature, give it +2/+2.', N'Lucien-Lachance.jpg', N'Lucien-Lachance_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (48, N'Midnight Sweep', 1, 2, 0, 0, 4, 2, NULL, N'Prophecy Summon a 2/2 Colovian Trooper with Guard in each lane.', N'Midnight-Sweep.jpg', N'Midnight-Sweep_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (49, N'Northpoint Captain', 5, 2, 4, 2, 4, 2, NULL, N'Summon: Summon a 0/4 Northpoint Herald with Guard.', N'Northpoint-Captain.jpg', N'Northpoint-Captain_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (50, N'Northpoint Lieutenant', 5, 2, 4, 2, 4, 2, NULL, NULL, N'Northpoint-Lieutenant.jpg', N'Northpoint-Lieutenant_thumb.jpg')
GO
INSERT [dbo].[Card] ([CardID], [CardName], [CardTypeID], [AttributeID], [Attack], [Defense], [ManaCost], [RarityID], [FlavorLoreText], [CardDescription], [ArtFullResFileName], [ArtThumbnailFileName]) VALUES (51, N'Preserver of the Root', 4, 2, 4, 4, 4, 3, NULL, N'While you have 7 or more max magicka, Preserver of the root has +2/+2 and Guard.', N'Preserver-of-the-Root.jpg', N'Preserver-of-the-Root_thumb.jpg')
GO
SET IDENTITY_INSERT [dbo].[Card] OFF
GO


