﻿CREATE TABLE [dbo].[DeckToCardsMap]
(
    [DeckToCardsMapID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [DeckID] INT NOT NULL, 
    [CardID] INT NOT NULL, 
    CONSTRAINT [FK_DeckToCardsMap_Deck_DeckID] FOREIGN KEY ([DeckID]) REFERENCES [Deck]([DeckID]), 
    CONSTRAINT [FK_DeckToCardsMap_Card_CardID] FOREIGN KEY ([CardID]) REFERENCES [Card]([CardID])
)
