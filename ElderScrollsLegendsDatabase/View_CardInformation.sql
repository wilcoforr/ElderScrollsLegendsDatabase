﻿CREATE VIEW [dbo].[CardInformation]
	AS SELECT CardName,
     Attribute.AttributeType,
     CardType.CardTypeValue, 
    Card.CardDescription,
    Card.ManaCost,
    Card.Attack,
    Card.Defense,
     Rarity.RarityType,
    Card.ArtFullResFileName,
    card.ArtThumbnailFileName
FROM Card INNER JOIN
CardType ON CardType.CardTypeID = Card.CardTypeID INNER JOIN
Attribute ON Attribute.AttributeID = Card.AttributeID INNER JOIN
Rarity ON Rarity.RarityID = Card.RarityID
