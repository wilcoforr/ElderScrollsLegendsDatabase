﻿USE [ElderScrollsLegendsDatabase]
GO

/****** Object: Table [dbo].[CardType] Script Date: 5/6/2016 11:16:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE [dbo].[CardType];


GO
CREATE TABLE [dbo].[CardType] (
    [CardTypeID] INT           IDENTITY (1, 1) NOT NULL,
    [CardType]   NVARCHAR (50) NULL
);


