﻿--SELECT * FROM Deck
--SELECT * FROM DeckToCardsMap

INSERT INTO Deck
(
    DeckName
    ,DeckDescription
    ,DateCreated
    ,DateLastModified
    ,UserID
)
VALUES
(
    'Crusader Deck'
    ,'Deck Description 2000 char'
    ,GETDATE() - 5
    ,GETDATE()
    ,1
),
(
    'Mage Deck'
    ,'Mage deck desc'
    ,GETDATE()-13
    ,GETDATE()-3
    ,1
)
-- test data for adding more decks - remove this if get errors
,
(
    'Archer Deck'
    ,'Archer deck desc'
    ,GETDATE()-2
    ,GETDATE()-1
    ,2
)
,
(
    'Berserker Deck'
    ,'Berserker deck desc'
    ,GETDATE()-6
    ,GETDATE()-3
    ,3
)
,
(
    'Poison Deck'
    ,'Poison deck desc'
    ,GETDATE()-8
    ,GETDATE()-2
    ,4
)
,
(
    'Life Drain Deck'
    ,'Life Drain deck desc'
    ,GETDATE()-7
    ,GETDATE()
    ,4
)
,
(
    'Spellblade Deck'
    ,'Spellblade deck desc'
    ,GETDATE()-10
    ,GETDATE()-5
    ,5
)
,
(
    'Elf Only Deck'
    ,'Elf Only deck desc'
    ,GETDATE()-12
    ,GETDATE()-1
    ,5
)


INSERT INTO DeckToCardsMap
(
    DeckID
    ,CardID
)
VALUES
(
    1
    ,1
),
(
    1
    ,2
),
(
    1
    ,3
),
(
    1
    ,4
),
(
    1
    ,5
),
(
    1
    ,6
),
(
    1
    ,6
),
(
    1
    ,7
),
(
    1
    ,7
),
(
--MageDeck
    2
    ,10
),
(
    2
    ,10
),
(
    2
    ,11
),
(
    2
    ,12
),
(
    2
    ,12
)





