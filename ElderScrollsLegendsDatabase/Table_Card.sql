﻿CREATE TABLE [dbo].[Card]
(
    [CardID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CardName] NVARCHAR(50) NULL, 
    [CardTypeID] INT NOT NULL, 
    [AttributeID] INT NOT NULL, 
    [Attack] INT NULL, 
    [Defense] INT NULL, 
    [ManaCost] INT NULL, 
    [RarityID] INT NOT NULL, 
    [FlavorLoreText] NVARCHAR(200) NULL, 
    [CardDescription] NVARCHAR(200) NULL, 
    [ArtFullResFileName] NVARCHAR(100) NULL,
    [ArtThumbnailFileName] NVARCHAR(100) NULL,
    CONSTRAINT [FK_Card_Rarity_RarityID] FOREIGN KEY ([RarityID]) REFERENCES [Rarity] ([RarityID]),
    CONSTRAINT [FK_Card_Attribute_AttributeID] FOREIGN KEY ([AttributeID]) REFERENCES [Attribute] ([AttributeID]), 
    CONSTRAINT [FK_Card_CardType_CardTypeID] FOREIGN KEY ([CardTypeID]) REFERENCES [CardType]([CardTypeID])
)
