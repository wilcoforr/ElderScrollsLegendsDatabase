﻿INSERT INTO [User]
(
    Username
    ,[Password]
    ,Email
    ,DateCreated
)
VALUES
(
    'Jaredth'
    ,'jaredthpass'
    ,'jaredth@example.com'
    ,GETDATE()
),
(
    'Seymour'
    ,'seymourpass'
    ,'seymour@example.com'
    ,GETDATE() - 5
),
(
    'Tidus'
    ,'tiduspass'
    ,'tidus@example.com'
    ,GETDATE() - 20
),
(
    'Yuna'
    ,'yunapass'
    ,'yuna@example.com'
    ,GETDATE() - 15
),
(
    'Wakka'
    ,'wakkapass'
    ,'wakka@example.com'
    ,GETDATE() - 11
),
(
    'Auron'
    ,'auronpass'
    ,'auron@example.com'
    ,GETDATE() - 8
),
(
    'Rikku'
    ,'rikkupass'
    ,'rikku@example.com'
    ,GETDATE() - 81
)
