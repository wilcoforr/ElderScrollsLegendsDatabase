﻿INSERT INTO Rarity
(
    RarityType
)
VALUES
(
    'Common'
),
(
    'Uncommon'
),
(
    'Rare'
),
(
    'Legendary'
),
(
    'Hero'
),
(
    'Holographic Rare'
),
(
    'Holographic Legendary'
)

