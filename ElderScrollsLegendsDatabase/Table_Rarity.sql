﻿CREATE TABLE [dbo].[Rarity]
(
    [RarityID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [RarityType] NVARCHAR(25) NULL
)
