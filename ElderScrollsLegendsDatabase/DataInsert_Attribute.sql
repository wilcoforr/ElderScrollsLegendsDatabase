﻿
--SELECT * FROM Attribute

INSERT INTO Attribute
(
    AttributeType
)
VALUES
(
    'Agility'
),
(
    'Endurance'
),
(
    'Intelligence'
),
(
    'Strength'
),
(
    'Willpower'
),
(
    'Neutral'
),
(
    'Agility-Endurance'
),
(
    'Agility-Intelligence'
),
(
    'Agility-Strength'
),
(
    'Agility-Willpower'
),
(
    'Endurance-Intelligence'
),
(
    'Endurance-Strength'
),
(
    'Endurance-Willpower'
),
(
    'Intelligence-Strength'
),
(
    'Intelligence-Willpower'
),
(
    'Strength-Willpower'
)




