﻿CREATE TABLE [dbo].[Deck]
(
    [DeckID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [DeckName] NVARCHAR(100) NOT NULL, 
    [DeckDescription] NVARCHAR(2000) NOT NULL, 
    [UserID] INT NOT NULL, 
    [DateCreated] DATETIME NOT NULL, 
    [DateLastModified] DATETIME NULL,
    CONSTRAINT [FK_Deck_User_UserID] FOREIGN KEY ([UserID]) REFERENCES [User]([UserID])

)
