﻿USE [ElderScrollsLegendsDatabase]
GO

/****** Object: Table [dbo].[Card] Script Date: 5/6/2016 9:36:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE [dbo].[Card];


GO
CREATE TABLE [dbo].[Card] (
    [CardID]               INT            IDENTITY (1, 1) NOT NULL,
    [CardName]             NVARCHAR (50)  NULL,
    [CardTypeID]           INT            NULL,
    [AttributeID]          INT            NULL,
    [Attack]               INT            NULL,
    [Defense]              INT            NULL,
    [ManaCost]             INT            NULL,
    [RarityID]             INT            NULL,
    [FlavorLoreText]       NVARCHAR (200) NULL,
    [CardDescription]      NVARCHAR (100)  NULL,
    [ArtThumbnailFileName] NVARCHAR (100) NULL,
    [ArtFullResFileName]   NVARCHAR (100) NULL
);


