﻿CREATE TABLE [dbo].[Attribute]
(
    [AttributeID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [AttributeType] NVARCHAR(25) NULL
)
