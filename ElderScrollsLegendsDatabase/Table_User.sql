﻿CREATE TABLE [dbo].[User]
(
    [UserID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [Username] NVARCHAR(20) NOT NULL,
    [Password] NVARCHAR(20) NOT NULL,
    [Email] NVARCHAR(100) NOT NULL, 
    [DateCreated] DATETIME NOT NULL
)
