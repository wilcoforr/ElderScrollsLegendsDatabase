﻿CREATE TABLE [dbo].[CardType]
(
    [CardTypeID] INT NOT NULL PRIMARY KEY IDENTITY, 
    [CardTypeValue] NVARCHAR(200) NULL
)
