﻿
SELECT * FROM Attribute
SELECT * FROM CardType
SELECT * FROM Rarity

SELECT * FROM [Card]
WHERE AttributeID = 1

SELECT * FROM [Card]
WHERE CardName LIKE '%Dragon%'

SELECT * FROM [Card]
WHERE CardTypeID = 10

SELECT * FROM [Card]
WHERE CardDescription != '' AND CardDescription IS NOT NULL

SELECT * FROM CardInformation





SELECT * FROM Deck
SELECT * FROM DeckToCardsMap

SELECT Card.CardName, Deck.DeckName
FROM Card INNER JOIN
    DeckToCardsMap ON DeckToCardsMap.CardID = Card.CardID INNER JOIN
    Deck ON Deck.DeckID = DeckToCardsMap.DeckID
WHERE DeckToCardsMap.DeckID = 1


ALTER TABLE Card
ADD CONSTRAINT [FK_Card_Rarity_RarityID] FOREIGN KEY ([RarityID]) REFERENCES [Rarity] ([RarityID])

ALTER TABLE Card
ADD CONSTRAINT [FK_Card_Attribute_AttributeID] FOREIGN KEY ([AttributeID]) REFERENCES [Attribute] ([AttributeID])




