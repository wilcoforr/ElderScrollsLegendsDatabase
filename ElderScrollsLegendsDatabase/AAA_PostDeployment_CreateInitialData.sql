﻿/*
Post-Deployment Script Template                            
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.        
 Use SQLCMD syntax to include a file in the post-deployment script.            
 Example:      :r .\myfile.sql                                
 Use SQLCMD syntax to reference a variable in the post-deployment script.        
 Example:      :setvar TableName MyTable                            
               SELECT * FROM [$(TableName)]                    
--------------------------------------------------------------------------------------
*/


PRINT('=================================================')
PRINT('Running DataInsert_Attribute.sql')
:r .\DataInsert_Attribute.sql

GO

PRINT('=================================================')
PRINT('Running DataInsert_CardType.sql')
:r .\DataInsert_CardType.sql

GO

PRINT('=================================================')
PRINT('Running DataInsert_Rarity.sql')
:r .\DataInsert_Rarity.sql


GO

PRINT('=================================================')
PRINT('Running DataInsert_Cards.sql')
:r .\DataInsert_Cards.sql

GO

PRINT('=================================================')
PRINT('Running DataInsert_Cards.sql')
:r .\DataInsert_DeckToCardsMap_TestData.sql

PRINT('=================================================')
PRINT('Running DataInsert_User.sql')
:r .\DataInsert_User.sql


PRINT('=================================================')
PRINT('=================================================')
PRINT('====================  DONE  =====================')
PRINT('=================================================')
PRINT('=================================================')

