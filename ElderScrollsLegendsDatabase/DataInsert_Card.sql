﻿USE [ElderScrollsLegendsDatabase]
GO

/****** Object: Table [dbo].[Card] Script Date: 5/6/2016 9:36:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

DROP TABLE [dbo].[Card];


GO
CREATE TABLE [dbo].[Card] (
    [CardID]               INT            IDENTITY (1, 1) NOT NULL,
    [CardName]             NVARCHAR (50)  NULL,
    [CardTypeID]           INT            NULL,
    [AttributeID]          INT            NULL,
    [Attack]               INT            NULL,
    [Defense]              INT            NULL,
    [ManaCost]             INT            NULL,
    [RarityID]             INT            NULL,
    [FlavorLoreText]       NVARCHAR (200) NULL,
    [CardDescription]      NVARCHAR (200)  NULL,
    [ArtThumbnailFileName] NVARCHAR (100) NULL,
    [ArtFullResFileName]   NVARCHAR (100) NULL
);



-- SELECT * FROM Card

INSERT INTO CARD
(
    CardName
    ,CardTypeID
    ,AttributeID
    ,Attack 
    ,Defense
    ,ManaCost
    ,RarityID
    ,FlavorLoreText
    ,CardDescription
    ,ArtThumbnailFileName
    ,ArtFullResFileName
)
VALUES
(
    'Nord Firebrand' --CardName
    ,10 --CardTypeID
    ,4 --AttributeID
    ,1 --Attack
    ,2 --Defense
    ,0 --Manacost
    ,0 --RarityID
    ,'' --FlavorLoreText
    ,'Charge' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Covenant Marauder' --CardName
    ,11 --CardTypeID
    ,4 --AttributeID
    ,2 --Attack
    ,1 --Defense
    ,1 --Manacost
    ,2 --RarityID
    ,'' --FlavorLoreText
    ,'+2/+0 when you have no cards in your hand.' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Fiery Imp' --CardName
    ,15 --CardTypeID
    ,4 --AttributeID
    ,1 --Attack
    ,1 --Defense
    ,1 --Manacost
    ,1 --RarityID
    ,'' --FlavorLoreText
    ,'When Fiery Imp is summoned, deal 2 damage to your opponent.' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Intimidate' --CardName
    ,1 --CardTypeID
    ,4 --AttributeID
    ,0 --Attack
    ,0 --Defense
    ,1 --Manacost
    ,1 --RarityID
    ,'' --FlavorLoreText
    ,'Enemy creatures lose Guard.' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Morthal Watchman' --CardName
    ,10 --CardTypeID
    ,4 --AttributeID
    ,2 --Attack
    ,1 --Defense
    ,1 --Manacost
    ,1 --RarityID
    ,'' --FlavorLoreText
    ,'' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Rapid Shot' --CardName
    ,1 --CardTypeID
    ,4 --AttributeID
    ,0 --Attack
    ,0 --Defense
    ,1 --Manacost
    ,1 --RarityID
    ,'' --FlavorLoreText
    ,'Deal 1 damage to a creature. If it survives, draw a card.' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Relentless Raider' --CardName
    ,10 --CardTypeID
    ,4 --AttributeID
    ,2 --Attack
    ,1 --Defense
    ,1 --Manacost
    ,3 --RarityID
    ,'' --FlavorLoreText
    ,'When an enemy rune is destroyed, deal 1 damage to your opponent.' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Scuttler' --CardName
    ,14 --CardTypeID
    ,4 --AttributeID
    ,1 --Attack
    ,2 --Defense
    ,1 --Manacost
    ,1 --RarityID
    ,'' --FlavorLoreText
    ,'' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Sharpshooter Scout' --CardName
    ,13 --CardTypeID
    ,4 --AttributeID
    ,1 --Attack
    ,1 --Defense
    ,1 --Manacost
    ,1 --RarityID
    ,'' --FlavorLoreText
    ,'Prophecy Summon: Deal 1 damage.' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Steel Scimitar' --CardName
    ,2 --CardTypeID
    ,4 --AttributeID
    ,1 --Attack
    ,1 --Defense
    ,1 --Manacost
    ,1 --RarityID
    ,'' --FlavorLoreText
    ,'+2/+2' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Blood Dragon' --CardName
    ,16 --CardTypeID
    ,4 --AttributeID
    ,5 --Attack
    ,7 --Defense
    ,5 --Manacost
    ,4 --RarityID
    ,'' --FlavorLoreText
    ,'Blood dragon ignores Guards and can attack creatures in any lane.' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Fortress Watchman' --CardName
    ,11 --CardTypeID
    ,4 --AttributeID
    ,5 --Attack
    ,4 --Defense
    ,5 --Manacost
    ,2 --RarityID
    ,'' --FlavorLoreText
    ,'Guard.' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Grahtwood Ambusher' --CardName
    ,13 --CardTypeID
    ,4 --AttributeID
    ,4 --Attack
    ,2 --Defense
    ,5 --Manacost
    ,3 --RarityID
    ,'' --FlavorLoreText
    ,'Prophecy Summon: Deal 1 damage to all enemy creatures in this lane.' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Markarth Bannerman' --CardName
    ,10 --CardTypeID
    ,4 --AttributeID
    ,4 --Attack
    ,4 --Defense
    ,5 --Manacost
    ,3 --RarityID
    ,'' --FlavorLoreText
    ,'When Markarth Bannerman attacks, draw two  1/1 Nord Firebrands with Charge.' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Reive, Blademaster' --CardName
    ,15 --CardTypeID
    ,4 --AttributeID
    ,4 --Attack
    ,4 --Defense
    ,5 --Manacost
    ,4 --RarityID
    ,'' --FlavorLoreText
    ,'Summon: The next item you equip this turn is free. Reive has a +1/+1 for each item he has equipped.' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Triumphant Jarl' --CardName
    ,10 --CardTypeID
    ,4 --AttributeID
    ,4 --Attack
    ,4 --Defense
    ,5 --Manacost
    ,3 --RarityID
    ,'' --FlavorLoreText
    ,'Summon: If you have more health than your opponent, draw two cards.' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Wood Orc Headhunter' --CardName
    ,11 --CardTypeID
    ,4 --AttributeID
    ,5 --Attack
    ,4 --Defense
    ,5 --Manacost
    ,3 --RarityID
    ,'' --FlavorLoreText
    ,'Breakthrough Summon: Give Wood Orc Headhunter Charge if you have another friendly orc.' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Burn and Pillage' --CardName
    ,1 --CardTypeID
    ,4 --AttributeID
    ,0 --Attack
    ,0 --Defense
    ,6 --Manacost
    ,4 --RarityID
    ,'' --FlavorLoreText
    ,'Deal 1 damage to all enemy creatures for each destroyed enemy rune.' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
),
(
    'Child of Hircine' --CardName
    ,19 --CardTypeID
    ,4 --AttributeID
    ,6 --Attack
    ,7 --Defense
    ,6 --Manacost
    ,3 --RarityID
    ,'' --FlavorLoreText
    ,'When Child of Hircine attacks and destroys a creature, it may attack again this turn.' --CardDescription
    ,'' --ArtThumb
    ,'' --ArtFull
)





